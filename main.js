// Assignment 4

function charReverseHandler(string) {
    return string.split('').map(char => char === char.toUpperCase() ? char.toLowerCase() : char.toUpperCase()).join("");
}


function createListHandler() {
    const container = document.querySelector('#tasks')
    const task = document.createElement("div");
    task.classList.add('task');

    const checkbox = document.createElement("input");
    checkbox.classList.add('checkbox')
    checkbox.type = "checkbox"

    const text = document.createTextNode(document.querySelector("#task").value);

    const button = document.createElement("button");
    button.classList.add('taskdelete')
    button.append(document.createTextNode("X"))

    task.append(checkbox)
    task.append(text)
    task.append(button)
    container.prepend(task)
}

function findDublicatedHandler(array) {
    const count = {}
    array.forEach(element => {
        count[element] = (count[element] ? count[element] : 0) + 1
    });
    return count
}

function unionHandler(arr, arr2) {
    return arr.concat(arr2)
}
const findTrue = (arr) => {
    return arr.filter(Boolean)
}

const reverseStringHandler = (string) => string.split('')
    .map((elem, i) => ({ index: i, lower: elem.toLowerCase(), former: elem }))
    .sort((a, b) => a.lower > b.lower ? 1 : a.lower < b.lower ? -1 : 0)
    .map(letter => letter.former)
    .join('')
